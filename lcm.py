# Write a Python program to find the least common multiple (LCM)
#  of two positive integers.


# define the function

def lcm(x,y):
    # check for the biggest number btn x and y
    # the bigger number is stored in some other variable like z

    if x>y:
        z=x
    else:
        z=y

# use the while loop 
# check whether z is divisible by x and y
    while True:
        # validation
        if (z%x==0) and (z%y==0):
            # the lcm is z
            return z
            break

        # if the condtions are not mett, then increment the value of z by 1
        else:
            z+=1

# call the function
print(lcm(2,2))