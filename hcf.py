# Write a Python program that computes the greatest common divisor (GCD)
#  of two positive integers.

def hcf (x,y):
    #initialize the hcf to 1
    hcf=1

    # check if x and y are divisible by @ other

    if (x%y==0):
        return y
    
    # iterate from half of y to o

    for i in range(int(y/2),-1,-1):
        # check if both x and y are divisible by i

        if (x%i==0) and (y%i==0):
            hcf=i
            break
    return hcf
print(hcf(4,6))